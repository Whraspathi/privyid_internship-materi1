/// <reference types="cypress" />

describe('Automation Test', () => {
    it('Sign and Share Task', () => {
        const file = "Test_Sign_and_Share.pdf"

        cy.visit('https://app.privy.id')
        cy.get('#__BVID__4').type("W10134572")
        cy.get(':nth-child(4) > .btn').click()
        cy.get('#__BVID__6').type('Crocodile717')
        cy.get('.form-group.mt-5 > .btn').click()
        cy.get('#v-step-0').click()
        cy.contains("Request From Others").click()
        cy.get('[type="file"]').attachFile({filePath: file, encoding: "base64"})
        cy.contains('Drag your document here or click browse').click()
        cy.contains("button", "Upload")
        //cy.get('.modal-content .modal-footer button:contains("Upload")').click()
        cy.wait(5000)
        cy.contains("Continue").click()
        cy.contains("Click to add recipient").click()
        cy.get('[placeholder="Enter PrivyID"]').type('GA4738', {force: true})
        cy.wait(3000)
        cy.contains("GA4738").click()
        cy.wait(3000)
        cy.get('[type="radio"]').check({force: true})
        cy.wait(3000)
        cy.contains("- Select Role - ").click()
        cy.wait(3000)
        cy.contains("Signer").click()
        cy.wait(3000)
        cy.contains("button", "Add recipient").click()
        cy.wait(3000)
        cy.contains("button", "Continue").click()
        cy.wait(3000)
        cy.contains("button", "Set Signature").click()
        cy.contains("Anggara Catra Prabaswara").click()
        cy.contains("Done").click()




    })
})
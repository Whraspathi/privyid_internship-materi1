/// <reference types="cypress" />

describe('Automation Test', () => {
    it('Change image signature Task', () => {
        cy.visit('app.privy.id')
        cy.get('input[name="user[privyId]"]').type('W10134572')
        cy.contains('CONTINUE').click()
        cy.get('input[name="user[secret]"]').type('Crocodile717')
        cy.contains('CONTINUE').click()
        cy.contains('Change My Signature Image').click()
        cy.contains('Add Signature').click()
        cy.get('input[id="name-initial"]').clear()
        cy.get('input[id="name-initial"]').type('Whraspathi')
        cy.contains('Save').click()
    })
})